package com.jeanson.solene.helloworld

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class NutritionFacts (val energy: NutritionFactsItem?,
                      val lipids: NutritionFactsItem?,
                      val saturatedFat: NutritionFactsItem?,
                      val calories: NutritionFactsItem?,
                      val carbohydrate: NutritionFactsItem?,
                      val sugar: NutritionFactsItem?,
                      val fiber: NutritionFactsItem?,
                      val proteins: NutritionFactsItem?,
                      val salt: NutritionFactsItem?,
                      val sodium: NutritionFactsItem?) : Parcelable