package com.jeanson.solene.helloworld.detailsPage

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.jeanson.solene.helloworld.NetworkManager
import com.jeanson.solene.helloworld.Product
import com.jeanson.solene.helloworld.R
import kotlinx.android.synthetic.main.product_details_activity.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ProductDetailActivity : AppCompatActivity() {

    private lateinit var barcode: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (!intent.hasExtra("product")){
            throw Exception("Product is missing")
        }

        barcode = intent.getStringExtra("product")

        setContentView(R.layout.product_details_activity)
        setSupportActionBar(toolbar)
        supportActionBar?.setTitle(R.string.toolbar_title_details)

        GlobalScope.launch(Dispatchers.Main) {
            try {

                pager.visibility = View.GONE
                progress_bar.visibility = View.VISIBLE

                val product = withContext(Dispatchers.IO) {
                    NetworkManager.getProduct(barcode)
                }

                if (product != null && product.code.isNotEmpty()) {
                    pager.adapter = ProductDetailsAdapter(
                        supportFragmentManager,
                        this@ProductDetailActivity,
                        product!!
                    )

                    tabs.setupWithViewPager(pager)

                    progress_bar.visibility = View.GONE
                    pager.visibility = View.VISIBLE

                    val database = FirebaseDatabase.getInstance()

                    val id = product.code



                    val listener = object : ValueEventListener{
                        override fun onDataChange(p0: DataSnapshot) {

                            var isFav = false

                            if (p0.child("fav").getValue(Boolean::class.java) != null){
                                isFav = p0.child("fav").getValue(Boolean::class.java)!!
                            }

                            val history = product.toHistory(isFav)

                            database.getReference("Users/${FirebaseAuth.getInstance().currentUser?.uid}/history/$id").setValue(history)
                        }

                        override fun onCancelled(p0: DatabaseError) {}
                    }

                    database.getReference(
                        "Users/${FirebaseAuth.getInstance().currentUser?.uid}/history/$id")
                        .addListenerForSingleValueEvent(listener)


                } else {
                    error_message.text = getString(R.string.error_message_not_found)
                    error_message.visibility = View.VISIBLE
                    progress_bar.visibility = View.GONE
                }

            } catch (e: Exception){}
        }
    }

    class ProductDetailsAdapter(fm: FragmentManager, val context: Context, private val product: Product) : FragmentPagerAdapter(fm){
        override fun getCount(): Int = 3

        override fun getItem(position: Int): Fragment {
            when (position) {
                0 -> return ProductDetailsFragment.newInstance(product)
                1 -> return ProductNutritionFragment.newInstance(product)
                2 -> return ProductNutritionInfosFragment.newInstance(product)
                else -> throw Exception("NOOOOOOO")
            }
        }

        override fun getPageTitle(position: Int): CharSequence? {

            when (position) {
                0 -> return context.getString(R.string.detailsToolbarTitle)
                1 -> return context.getString(R.string.nutritionToolbarTitle)
                2 -> return context.getString(R.string.nutritionInfosToolbarTitle)
                else -> throw Exception("NOOOOOOO")
            }
        }

    }
}

