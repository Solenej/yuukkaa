package com.jeanson.solene.helloworld.mainPage

import android.graphics.PorterDuff
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.jeanson.solene.helloworld.Product
import com.jeanson.solene.helloworld.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_product_layout.view.*

class ItemListViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val productName: TextView = view.item_product_name
    val productBrand: TextView = view.item_product_brand
    val productNutriscore: TextView = view.item_product_nutriscore_txt
    val productCalories: TextView = view.item_product_calories_txt
    val productImage: ImageView = view.item_product_image
    val productBookmark: ImageView = view.item_product_bookmark_img
    val productCardView: CardView = view.item_product_cardview



    fun bindProduct(productWithFav: ProductWithFav, listener: OnProductClickListener){
        val product = productWithFav.product

        val calories = product.nutritionsFacts?.calories?.quantityFor100g
        val nutriscore = product.nutriscore?.toUpperCase()

        Log.e("brands", product.brand?.toString())

        productName.text = product.name
        productBrand.text = product.brand?.joinToString()
        productNutriscore.text = itemView.resources.getString(R.string.item_product_nutriscore, nutriscore ?: itemView.resources.getString(R.string.nutriscore_not_given))
        productCalories.text = itemView.resources.getString(R.string.item_product_calories, calories ?: 0)
        Picasso.get().load(product.imageUrl).into(productImage)

        if (productWithFav.fav) {
            productBookmark.setColorFilter(
                ContextCompat.getColor( itemView.context, R.color.icon_selected),
                PorterDuff.Mode.SRC_ATOP
            )
        } else {
            productBookmark.setColorFilter(
                ContextCompat.getColor(itemView.context, R.color.icon_default),
                PorterDuff.Mode.SRC_ATOP
            )
        }

        productCardView.setOnClickListener{
            listener.onProductClicked(product.code)
        }

        productBookmark.setOnClickListener{
            listener.onBookmarkClicked(productWithFav)
        }
    }
}

data class ProductWithFav(
    val product: Product,
    val fav: Boolean
)