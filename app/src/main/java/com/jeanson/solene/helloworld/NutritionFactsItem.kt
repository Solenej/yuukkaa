package com.jeanson.solene.helloworld

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class NutritionFactsItem(val unit: String?,
                         val quantityByPart: String?,
                         val quantityFor100g: String?) : Parcelable