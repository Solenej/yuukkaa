package com.jeanson.solene.helloworld.mainPage

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.jeanson.solene.helloworld.R
import com.jeanson.solene.helloworld.detailsPage.ProductDetailActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        supportActionBar?.setBackgroundDrawable(
            ContextCompat.getDrawable(
                this,
                R.drawable.toolbar_gradient
            )
        )
        supportActionBar?.setTitle(R.string.toolbar_title_products)

        main_navigation.setOnNavigationItemSelectedListener {

            when (it.itemId) {
                R.id.main_navbar_history -> supportFragmentManager.beginTransaction().replace(
                    R.id.main_content,
                    HistoryFragment()
                ).commitAllowingStateLoss()
                R.id.main_navbar_favs -> supportFragmentManager.beginTransaction().replace(
                    R.id.main_content,
                    FavoriteProductsFragment()
                ).commitAllowingStateLoss()
                R.id.main_navbar_stats -> supportFragmentManager.beginTransaction().replace(
                    R.id.main_content,
                    StatisticsFragment()
                ).commitAllowingStateLoss()
                R.id.main_navbar_profile -> supportFragmentManager.beginTransaction().replace(
                    R.id.main_content,
                    ProfileFragment()
                ).commitAllowingStateLoss()
                else -> supportFragmentManager.beginTransaction().replace(
                    R.id.main_content,
                    HistoryFragment()
                ).commitAllowingStateLoss()
            }
            true
        }
        main_navigation.selectedItemId = R.id.main_navbar_history


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.bar_code, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.icon_menu -> {

                //lauch read barre code
                startActivityForResult(Intent("com.google.zxing.client.android.SCAN").apply {
                    putExtra("SCAN_FORMATS", "EAN_13")
                }, 100)

                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (data == null) {
            throw Exception("Product not found")
        } else {
            if (data!!.hasExtra("SCAN_RESULT_FORMAT") && data!!.hasExtra("SCAN_RESULT")) {
                val barcode = data?.getStringExtra("SCAN_RESULT")

                var intent = Intent(this, ProductDetailActivity::class.java)
                intent.putExtra("product", barcode)
                startActivity(intent)
            }
        }

    }
}


