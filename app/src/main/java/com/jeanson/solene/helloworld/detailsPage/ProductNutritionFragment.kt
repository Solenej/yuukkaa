package com.jeanson.solene.helloworld.detailsPage

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.Fragment
import com.jeanson.solene.helloworld.Product
import com.jeanson.solene.helloworld.R
import kotlinx.android.synthetic.main.nutrition_layout.*

class ProductNutritionFragment : Fragment() {

    private lateinit var product: Product

    companion object {
        fun newInstance(product: Product) : ProductNutritionFragment {
            val fragment = ProductNutritionFragment()
            val args = Bundle()
            args.putParcelable("product", product)

            fragment.arguments = args

            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.nutrition_layout, container, false)

    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments == null || arguments?.containsKey("product") == false){
            throw Exception("The product is missing")
        }

        this.product = arguments!!.getParcelable<Product>("product")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // initiate view values
        var colorLow = activity?.applicationContext?.getColor(R.color.nutrient_level_low)
        var colorMiddle = activity?.applicationContext?.getColor(R.color.nutrient_level_moderate)
        var colorHigh = activity?.applicationContext?.getColor(R.color.nutrient_level_high)

        nutrition_lipid.text = getString(R.string.nutrition_lipids, product.nutritionsFacts?.lipids?.quantityFor100g)
        nutrition_saturated_fat.text = getString(R.string.nutrition_saturated_fat, product.nutritionsFacts?.saturatedFat?.quantityFor100g)
        nutrition_sugar.text = getString(R.string.nutrition_sugar, product.nutritionsFacts?.sugar?.quantityFor100g)
        nutrition_salt.text = getString(R.string.nutrition_salt, product.nutritionsFacts?.salt?.quantityFor100g)

        if (product.nutritionsFacts?.lipids?.quantityFor100g?.toFloat()!! < 3.0){
            DrawableCompat.setTintList(nutrition_lipid_color.background, ColorStateList.valueOf(colorLow!!))
            nutrition_lipid_quantity.text = getString(R.string.nutrition_quantity_low)
        }else if (product.nutritionsFacts?.lipids?.quantityFor100g?.toFloat()!! in 3.0..20.0){
            DrawableCompat.setTintList(nutrition_lipid_color.background, ColorStateList.valueOf(colorMiddle!!))
            nutrition_lipid_quantity.text = getString(R.string.nutrition_quantity_low)

        }else{
            DrawableCompat.setTintList(nutrition_lipid_color.background, ColorStateList.valueOf(colorHigh!!))
            nutrition_lipid_quantity.text = getString(R.string.nutrition_quantity_low)

        }

        if (product.nutritionsFacts?.saturatedFat?.quantityFor100g?.toFloat()!! < 1.5){
            DrawableCompat.setTintList(nutrition_saturated_fat_color.background, ColorStateList.valueOf(colorLow!!))
            nutrition_saturated_fat_quantity.text = getString(R.string.nutrition_quantity_low)

        }else if (product.nutritionsFacts?.saturatedFat?.quantityFor100g?.toFloat()!! in 1.5..5.0){
            DrawableCompat.setTintList(nutrition_saturated_fat_color.background, ColorStateList.valueOf(colorMiddle!!))
            nutrition_saturated_fat_quantity.text = getString(R.string.nutrition_quantity_moderate)

        }else{
            DrawableCompat.setTintList(nutrition_saturated_fat_color.background, ColorStateList.valueOf(colorHigh!!))
            nutrition_saturated_fat_quantity.text = getString(R.string.nutrition_quantity_high)

        }

        if (product.nutritionsFacts?.sugar?.quantityFor100g?.toFloat()!! < 5.0){
            DrawableCompat.setTintList(nutrition_sugar_color.background, ColorStateList.valueOf(colorLow!!))
            nutrition_sugar_quantity.text = getString(R.string.nutrition_quantity_low)

        }else if (product.nutritionsFacts?.sugar?.quantityFor100g?.toFloat()!! in 5.0..12.5){
            DrawableCompat.setTintList(nutrition_sugar_color.background, ColorStateList.valueOf(colorMiddle!!))
            nutrition_sugar_quantity.text = getString(R.string.nutrition_quantity_moderate)

        }else{
            DrawableCompat.setTintList(nutrition_sugar_color.background, ColorStateList.valueOf(colorHigh!!))
            nutrition_sugar_quantity.text = getString(R.string.nutrition_quantity_high)

        }

        if (product.nutritionsFacts?.salt?.quantityFor100g?.toFloat()!! < 0.3){
            DrawableCompat.setTintList(nutrition_salt_color.background, ColorStateList.valueOf(colorLow!!))
            nutrition_salt_quantity.text = getString(R.string.nutrition_quantity_low)

        }else if (product.nutritionsFacts?.salt?.quantityFor100g?.toFloat()!! in 0.3..1.5){
            DrawableCompat.setTintList(nutrition_salt_color.background, ColorStateList.valueOf(colorMiddle!!))
            nutrition_salt_quantity.text = getString(R.string.nutrition_quantity_moderate)

        }else{
            DrawableCompat.setTintList(nutrition_salt_color.background, ColorStateList.valueOf(colorHigh!!))
            nutrition_salt_quantity.text = getString(R.string.nutrition_quantity_high)

        }
    }
}