package com.jeanson.solene.helloworld.detailsPage

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.jeanson.solene.helloworld.Product
import com.jeanson.solene.helloworld.R
import kotlinx.android.synthetic.main.nutrition_infos_layout.*

class ProductNutritionInfosFragment : Fragment() {

    private lateinit var product: Product

    companion object {
        fun newInstance(product: Product) : ProductNutritionInfosFragment {
            val fragment = ProductNutritionInfosFragment()
            val args = Bundle()
            args.putParcelable("product", product)

            fragment.arguments = args

            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.nutrition_infos_layout, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments == null || arguments?.containsKey("product") == false){
            throw Exception("The product is missing")
        }

        this.product = arguments!!.getParcelable<Product>("product")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Log.d("tag", product.name)

        // initiate view values

        energy_100_g.text = product.nutritionsFacts?.energy?.quantityFor100g
        energy_part.text = product.nutritionsFacts?.energy?.quantityByPart

        lipids_100_g.text = product.nutritionsFacts?.lipids?.quantityFor100g
        lipids_part.text = product.nutritionsFacts?.lipids?.quantityByPart

        saturated_fat_100_g.text = product.nutritionsFacts?.saturatedFat?.quantityFor100g
        saturated_fat_part.text = product.nutritionsFacts?.saturatedFat?.quantityByPart

        glucids_100_g.text = product.nutritionsFacts?.carbohydrate?.quantityFor100g
        glucids_part.text = product.nutritionsFacts?.carbohydrate?.quantityByPart

        sugar_100_g.text = product.nutritionsFacts?.sugar?.quantityFor100g
        sugar_part.text = product.nutritionsFacts?.sugar?.quantityByPart

        fiber_100_g.text = product.nutritionsFacts?.fiber?.quantityFor100g ?: "0"
        fiber_part.text = product.nutritionsFacts?.fiber?.quantityByPart ?: "0"

        proteins_100_g.text = product.nutritionsFacts?.proteins?.quantityFor100g
        proteins_part.text = product.nutritionsFacts?.proteins?.quantityByPart

        salt_100_g.text = product.nutritionsFacts?.salt?.quantityFor100g
        salt_part.text = product.nutritionsFacts?.salt?.quantityByPart

        sodium_100_g.text = product.nutritionsFacts?.sodium?.quantityFor100g
        sodium_part.text = product.nutritionsFacts?.sodium?.quantityByPart
    }
}