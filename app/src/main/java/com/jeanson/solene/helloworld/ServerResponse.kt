package com.jeanson.solene.helloworld

import com.google.gson.annotations.SerializedName

data class ServerResponse(
    @SerializedName("error")
    val error: String?,
    @SerializedName("response")
    val response: Response?
) {
    data class Response(
        @SerializedName("additives")
        val additives: Map<String, String>?,
        @SerializedName("allergens")
        val allergens: List<String?>?,
        @SerializedName("altName")
        val altName: String?,
        @SerializedName("barcode")
        val barcode: String,
        @SerializedName("brands")
        val brands: List<String?>?,
        @SerializedName("containsPalmOil")
        val containsPalmOil: Boolean?,
        @SerializedName("ingredients")
        val ingredients: List<String?>?,
        @SerializedName("manufacturingCountries")
        val manufacturingCountries: List<String?>?,
        @SerializedName("name")
        val name: String?,
        @SerializedName("novaScore")
        val novaScore: String?,
        @SerializedName("nutriScore")
        val nutriScore: String?,
        @SerializedName("nutritionFacts")
        val nutritionFacts: NutritionFactsResponse?,
        @SerializedName("picture")
        val picture: String?,
        @SerializedName("quantity")
        val quantity: String?,
        @SerializedName("traces")
        val traces: List<String?>?
    ){

        fun toProduct() : Product {

            return Product(
                name = name,
                brand = brands,
                code = barcode,
                nutriscore = nutriScore,
                imageUrl = picture,
                quantity = quantity,
                soldIn = manufacturingCountries,
                ingredients = ingredients,
                allergens = allergens,
                additives = additives,
                nutritionsFacts = nutritionFacts?.toNutritionFacts())
        }
    }

    data class NutritionFactsResponse(
        @SerializedName("calories")
        val calories: NutritionFactResponse?,
        @SerializedName("carbohydrate")
        val carbohydrate: NutritionFactResponse?,
        @SerializedName("energy")
        val energy: NutritionFactResponse?,
        @SerializedName("fat")
        val fat: NutritionFactResponse?,
        @SerializedName("fiber")
        val fiber: NutritionFactResponse?,
        @SerializedName("proteins")
        val proteins: NutritionFactResponse?,
        @SerializedName("salt")
        val salt: NutritionFactResponse?,
        @SerializedName("saturatedFat")
        val saturatedFat: NutritionFactResponse?,
        @SerializedName("servingSize")
        val servingSize: String?,
        @SerializedName("sodium")
        val sodium: NutritionFactResponse?,
        @SerializedName("sugar")
        val sugar: NutritionFactResponse?
    ) {
        data class NutritionFactResponse(
            @SerializedName("per100g")
            val per100g: String?,
            @SerializedName("perServing")
            val perServing: String?,
            @SerializedName("unit")
            val unit: String?
        ){
            fun toNutritionFactItem() : NutritionFactsItem{
                return NutritionFactsItem(
                    unit = unit,
                    quantityFor100g = per100g,
                    quantityByPart = perServing
                )
            }
        }

        fun toNutritionFacts() : NutritionFacts {
            return NutritionFacts(
                energy = energy?.toNutritionFactItem(),
                lipids = fat?.toNutritionFactItem(),
                fiber = fiber?.toNutritionFactItem(),
                saturatedFat = saturatedFat?.toNutritionFactItem(),
                carbohydrate = sugar?.toNutritionFactItem(),
                proteins = proteins?.toNutritionFactItem(),
                salt = salt?.toNutritionFactItem(),
                sodium = sodium?.toNutritionFactItem(),
                sugar = sugar?.toNutritionFactItem(),
                calories = calories?.toNutritionFactItem()
            )
        }
    }
}