package com.jeanson.solene.helloworld.authPage

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.jeanson.solene.helloworld.R

class AuthActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth_layout)

    }

    override fun onResume() {
        super.onResume()

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.main_content, SignInFragment())
            .commitAllowingStateLoss()
    }
}