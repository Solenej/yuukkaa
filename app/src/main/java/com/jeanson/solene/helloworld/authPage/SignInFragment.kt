package com.jeanson.solene.helloworld.authPage

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.Constraints.TAG
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.jeanson.solene.helloworld.R
import com.jeanson.solene.helloworld.mainPage.MainActivity
import kotlinx.android.synthetic.main.auth_signin_layout.*

class SignInFragment : Fragment(){

    private lateinit var auth: FirebaseAuth

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.auth_signin_layout, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

    }

    override fun onResume() {
        super.onResume()

        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        if (currentUser != null){
            val intent = Intent(activity?.applicationContext, MainActivity::class.java)
            startActivity(intent)
            activity?.finish()
        }

        sign_in_button.setOnClickListener {
            auth.signInWithEmailAndPassword(email_sign_in.text.toString(), password_sign_in.text.toString())
                .addOnCompleteListener(activity as AppCompatActivity) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithEmail:success")
                        val user = auth.currentUser

                        if (user != null){
                            val intent = Intent(activity?.applicationContext, MainActivity::class.java)
                            startActivity(intent)
                            activity?.finish()
                        }

                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithEmail:failure", task.exception)
                        Toast.makeText(activity?.baseContext, "Authentication failed.",
                            Toast.LENGTH_SHORT).show()

                        sign_in_error.visibility = View.VISIBLE
                        sign_in_error.text = getString(R.string.error_auth)
                    }
                }
        }

        no_account_link.setOnClickListener {
            activity?.supportFragmentManager!!.beginTransaction()
                .replace(R.id.main_content, SignUpFragment())
                .commitAllowingStateLoss()
        }

        password_lost.setOnClickListener {
            activity?.supportFragmentManager!!.beginTransaction()
                .replace(R.id.main_content, LostPwdFragment())
                .commitAllowingStateLoss()
        }

    }
}