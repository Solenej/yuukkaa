package com.jeanson.solene.helloworld.authPage

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.jeanson.solene.helloworld.R
import kotlinx.android.synthetic.main.lost_password_layout.*

class LostPwdFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.lost_password_layout, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun onResume() {
        super.onResume()

        val auth = FirebaseAuth.getInstance()

        send_password_button.setOnClickListener {
            auth.sendPasswordResetEmail(email_lost_pwd.text.toString())
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Log.d(TAG, "Email sent.")
                        activity?.supportFragmentManager!!.beginTransaction()
                            .replace(R.id.main_content, SignInFragment())
                            .commitAllowingStateLoss()
                    }
                }

        }

        sign_in_lost_pwd.setOnClickListener {
            activity?.supportFragmentManager!!.beginTransaction()
                .replace(R.id.main_content, SignInFragment())
                .commitAllowingStateLoss()
        }

    }
}