package com.jeanson.solene.helloworld

data class History(val code: String,
                   val brand:String?,
                   val imageURL: String?,
                   val name: String?,
                   val nutriscore: String?,
                   val fav: Boolean){

    constructor() : this("", null, null ,null, null, false)

    fun toProduct(): Product{
        return Product(
            name = name,
            brand = listOf(brand),
            code = code,
            nutriscore = nutriscore,
            imageUrl = imageURL,
            quantity = null,
            soldIn = null,
            ingredients = null,
            allergens = null,
            additives = null,
            nutritionsFacts = null
        )
    }
}