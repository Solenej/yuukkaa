package com.jeanson.solene.helloworld.mainPage

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.jeanson.solene.helloworld.History
import com.jeanson.solene.helloworld.R
import com.jeanson.solene.helloworld.authPage.AuthActivity
import kotlinx.android.synthetic.main.list_product_layout.*
import kotlinx.android.synthetic.main.profile_layout.*

class ProfileFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.profile_layout, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.setTitle(R.string.toolbar_title_profile)

    }

    override fun onResume() {
        super.onResume()
        val products = mutableListOf<ProductWithFav>()

        val listener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                for (historyItem in dataSnapshot.children) {
                    val item = historyItem.getValue(History::class.java)
                    val fav = historyItem.child("fav").getValue(Boolean::class.java)

                    if (item != null) {
                        val product = ProductWithFav(item.toProduct(), fav ?: false)
                        if (!products.contains(product)) {
                            products.add(product)
                        }
                    }
                    nb_products.text = resources.getString(R.string.profile_nb_products, products.size)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        }

        val uid = FirebaseAuth.getInstance().currentUser?.uid!!


        val reference = FirebaseDatabase.getInstance().getReference("Users/$uid/history")
        reference!!.addValueEventListener(listener!!)

        username.text = FirebaseAuth.getInstance().currentUser?.email
        sign_out_button.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            activity?.finish()
            val intent = Intent(activity?.applicationContext, AuthActivity::class.java)
            startActivity(intent)
        }
    }
}