package com.jeanson.solene.helloworld.mainPage

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jeanson.solene.helloworld.R

class ListAdapter(val products: List<ProductWithFav>, val listener: OnProductClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return ItemListViewHolder(
            inflater.inflate(
                R.layout.item_product_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ItemListViewHolder).bindProduct(products[position], listener )
    }

    override fun getItemCount(): Int {
        return products.size
    }
}