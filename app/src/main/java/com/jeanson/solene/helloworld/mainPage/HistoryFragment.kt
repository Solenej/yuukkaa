package com.jeanson.solene.helloworld.mainPage

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.jeanson.solene.helloworld.History
import com.jeanson.solene.helloworld.R
import com.jeanson.solene.helloworld.detailsPage.ProductDetailActivity
import kotlinx.android.synthetic.main.list_product_layout.*

class HistoryFragment : Fragment(), OnProductClickListener {

    var listener: ValueEventListener? = null
    var reference: DatabaseReference? = null

    private lateinit var uid: String


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.list_product_layout, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        list.layoutManager = LinearLayoutManager(activity?.applicationContext)
        (activity as AppCompatActivity).supportActionBar?.setTitle(R.string.toolbar_title_products)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (FirebaseAuth.getInstance().currentUser?.uid == null){
            throw Exception("Initialization failed: User not found")
        }

        uid = FirebaseAuth.getInstance().currentUser?.uid!!

    }

    override fun onResume() {
        super.onResume()

        val database = FirebaseDatabase.getInstance()

        listener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val products = mutableListOf<ProductWithFav>()

                for (historyItem in dataSnapshot.children) {
                    val item = historyItem.getValue(History::class.java)
                    val fav = historyItem.child("fav").getValue(Boolean::class.java)

                    if (item != null) {
                        val product = ProductWithFav(item.toProduct(), fav ?: false)
                        if (!products.contains(product)) {
                            products.add(product)
                        }
                    }
                }

                val adapter = ListAdapter(products, this@HistoryFragment)
                list.adapter = adapter

                if (products.isEmpty()) {
                    list.visibility = View.GONE
                    empty_list.visibility = View.VISIBLE

                } else {
                    list.visibility = View.VISIBLE
                    empty_list.visibility = View.GONE

                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        }

        reference = database.getReference("Users/$uid/history")
        reference!!.addValueEventListener(listener!!)

        button_scan.setOnClickListener {
            startActivityForResult(Intent("com.google.zxing.client.android.SCAN").apply {
                putExtra("SCAN_FORMATS", "EAN_13")
            }, 100)
        }

    }

    override fun onPause() {
        super.onPause()

        if (listener != null) {
            reference?.removeEventListener(listener!!)
        }
    }

    override fun onProductClicked(product: String) {
        var intent = Intent(activity?.applicationContext, ProductDetailActivity::class.java)
        intent.putExtra("product", product)
        startActivity(intent)
    }

    override fun onBookmarkClicked(productWithFav: ProductWithFav) {
        val product = productWithFav.product

        Toast.makeText(activity?.applicationContext, "BOOKMARK", Toast.LENGTH_LONG).show()
        val database = FirebaseDatabase.getInstance()
        val id = product.code

        // if bookmark not activated
        if (!productWithFav.fav) {
            val favorite = History(
                product.code,
                product.brand?.joinToString(),
                product.imageUrl,
                product.name,
                product.nutriscore,
                true
            )
            database.getReference("Users/$uid/favorites/$id").setValue(favorite)
            database.getReference("Users/$uid/history/$id/fav").setValue(true)
        } else {
            database.getReference("Users/$uid/favorites/$id").removeValue()
            database.getReference("Users/$uid/history/$id/fav").setValue(false)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (data == null) {
            throw Exception("Product not found")
        } else {
            if (data!!.hasExtra("SCAN_RESULT_FORMAT") && data!!.hasExtra("SCAN_RESULT")) {
                val barcode = data?.getStringExtra("SCAN_RESULT")

                var intent = Intent(activity?.applicationContext, ProductDetailActivity::class.java)
                intent.putExtra("product", barcode)
                startActivity(intent)
            }
        }

    }

}

interface OnProductClickListener {
    fun onProductClicked(product: String)
    fun onBookmarkClicked(product: ProductWithFav)
}