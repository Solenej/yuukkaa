package com.jeanson.solene.helloworld.mainPage

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.jeanson.solene.helloworld.R
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.jeanson.solene.helloworld.History
import com.jeanson.solene.helloworld.NetworkManager
import com.jeanson.solene.helloworld.Product
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.formatter.PercentFormatter


class StatisticsFragment : Fragment() {

    private var listener: ValueEventListener? = null
    private var reference: DatabaseReference? = null

    var goodProducts = mutableListOf<Product>()
    var mediumProducts = mutableListOf<Product>()
    var badProducts = mutableListOf<Product>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.statistics_layout, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.setTitle(R.string.toolbar_title_statistics)
    }

    override fun onResume() {
        super.onResume()

        val database = FirebaseDatabase.getInstance()
        val products = mutableListOf<Product>()

        listener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (historyItem in dataSnapshot.children) {
                    val item = historyItem.getValue(History::class.java)

                    if (item != null) {
                        val product = item.toProduct()
                        if (!products.contains(product)) {
                            products.add(product)
                        }
                    }
                }

                    GlobalScope.launch(Dispatchers.Main) {
                        try {
                            val chart = view?.findViewById(R.id.statsChart) as PieChart
                            chart.visibility = View.GONE

                            for (prod in products){
                                val product = withContext(Dispatchers.IO) {
                                    NetworkManager.getProduct(prod.code)
                                }

                                if (product?.nutritionsFacts !== null){
                                    if (
                                        product.nutritionsFacts.lipids?.quantityFor100g?.toFloat()!! < 3.0
                                        && product.nutritionsFacts.saturatedFat?.quantityFor100g?.toFloat()!! < 1.5
                                        && product.nutritionsFacts.sugar?.quantityFor100g?.toFloat()!! < 5.0 ||
                                        product.nutritionsFacts.lipids.quantityFor100g.toFloat() < 3.0
                                        && product.nutritionsFacts.saturatedFat?.quantityFor100g?.toFloat()!! < 1.5
                                        && product.nutritionsFacts.salt?.quantityFor100g?.toFloat()!! < 0.3 ||
                                        product.nutritionsFacts.saturatedFat?.quantityFor100g?.toFloat()!! < 1.5
                                        && product.nutritionsFacts.sugar?.quantityFor100g?.toFloat()!! < 5.0
                                        && product.nutritionsFacts.salt?.quantityFor100g?.toFloat()!! < 0.3
                                    ){
                                        goodProducts.add(product)
                                    }else if (
                                        product.nutritionsFacts.lipids.quantityFor100g.toFloat() > 20.0
                                        && product.nutritionsFacts.saturatedFat.quantityFor100g.toFloat() > 5.0
                                        && product.nutritionsFacts.sugar?.quantityFor100g?.toFloat()!! > 12.5 ||
                                        product.nutritionsFacts.lipids.quantityFor100g.toFloat() > 20.0
                                        && product.nutritionsFacts.saturatedFat.quantityFor100g.toFloat() > 5.0
                                        && product.nutritionsFacts.salt?.quantityFor100g?.toFloat()!! > 1.5 ||
                                        product.nutritionsFacts.saturatedFat.quantityFor100g.toFloat() > 5.0
                                        && product.nutritionsFacts.sugar?.quantityFor100g?.toFloat()!! < 12.5
                                        && product.nutritionsFacts.salt?.quantityFor100g?.toFloat()!! < 1.5
                                    ){
                                        badProducts.add(product)
                                    }else {
                                        mediumProducts.add(product)
                                    }
                                }
                            }

                            Log.e("good", goodProducts.toString())
                            Log.e("medium", mediumProducts.toString())
                            Log.e("bad", badProducts.toString())

                            val productsData = listOf(goodProducts.size, mediumProducts.size, badProducts.size)

                            val entries = ArrayList<PieEntry>()

                            entries.add(PieEntry(productsData[0].toFloat(), getString(R.string.good_products)))
                            entries.add(PieEntry(productsData[1].toFloat(), getString(R.string.medium_products)))
                            entries.add(PieEntry(productsData[2].toFloat(), getString(R.string.bad_products)))

                            val colors = ArrayList<Int>()

                            for (c in ColorTemplate.VORDIPLOM_COLORS)
                                colors.add(c)
                            for (c in ColorTemplate.JOYFUL_COLORS)
                                colors.add(c)
                            for (c in ColorTemplate.COLORFUL_COLORS)
                                colors.add(c)
                            for (c in ColorTemplate.LIBERTY_COLORS)
                                colors.add(c)
                            for (c in ColorTemplate.PASTEL_COLORS)
                                colors.add(c)

                            colors.add(ColorTemplate.getHoloBlue())

                            val dataSet = PieDataSet(entries, getString(R.string.stats_chart_name))
                            dataSet.colors = colors

                            val data = PieData(dataSet)
                            data.setValueTextSize(11f)

                            data.setValueTextColor(Color.BLACK)
                            data.setValueFormatter(PercentFormatter(chart))

                            val l = chart.legend
                            l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
                            l.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
                            l.orientation = Legend.LegendOrientation.VERTICAL
                            l.setDrawInside(false)
                            l.textColor = Color.BLACK
                            l.yEntrySpace = 5f

                            chart.data = data
                            chart.setUsePercentValues(true)
                            chart.description.isEnabled = false
                            chart.centerText = getString(R.string.stats_chart_name)
                            chart.setDrawCenterText(true)
                            chart.rotationAngle = 0.toFloat()
                            chart.isRotationEnabled = true
                            chart.isHighlightPerTapEnabled = true
                            chart.setEntryLabelColor(Color.BLACK)
                            chart.animateY(1400, Easing.EaseInOutQuad)
                            chart.setEntryLabelTextSize(12f)
                            chart.invalidate()

                            chart.visibility = View.VISIBLE

                        } catch (e: Exception){}
                    }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        }


        val uid = FirebaseAuth.getInstance().currentUser?.uid!!

        reference = database.getReference("Users/$uid/history")
        reference!!.addValueEventListener(listener!!)
    }
}