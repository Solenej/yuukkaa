package com.jeanson.solene.helloworld

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Product(val name: String?,
    val brand: List<String?>?,
    val code: String,
    val nutriscore: String?,
    val imageUrl: String?,
    val quantity: String?,
    val soldIn: List<String?>?,
    val ingredients: List<String?>?,
    val allergens: List<String?>?,
    val additives: Map<String, String>?,
    val nutritionsFacts : NutritionFacts?) : Parcelable {
    
    fun toHistory(fav: Boolean) : History{
        return History(code, brand?.joinToString(), imageUrl, name, nutriscore, fav)
        
    }
}

