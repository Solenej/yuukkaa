package com.jeanson.solene.helloworld.mainPage

import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.jeanson.solene.helloworld.History
import com.jeanson.solene.helloworld.R
import com.jeanson.solene.helloworld.detailsPage.ProductDetailActivity
import kotlinx.android.synthetic.main.item_product_layout.*
import kotlinx.android.synthetic.main.list_product_layout.*

class FavoriteProductsFragment : Fragment(), OnProductClickListener {

    var listener : ValueEventListener? = null
    var reference : DatabaseReference? = null

    private lateinit var uid: String

    override fun onProductClicked(product: String) {
        var intent = Intent(activity?.applicationContext, ProductDetailActivity::class.java)
        intent.putExtra("product", product)
        startActivity(intent)
    }

    override fun onBookmarkClicked(productWithFav: ProductWithFav) {
        val product = productWithFav.product
        val database = FirebaseDatabase.getInstance()
        val id = product.code
        item_product_bookmark_img.setColorFilter(ContextCompat.getColor(requireContext(), R.color.icon_default), PorterDuff.Mode.SRC_ATOP)
        database.getReference("Users/$uid/favorites/$id").removeValue()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.list_product_layout, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        list.layoutManager = LinearLayoutManager(activity?.applicationContext)
        (activity as AppCompatActivity).supportActionBar?.setTitle(R.string.toolbar_title_favorites)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (FirebaseAuth.getInstance().currentUser?.uid == null){
            throw Exception("Initialization failed: User not found")
        }

        uid = FirebaseAuth.getInstance().currentUser?.uid!!

    }

    override fun onResume() {
        super.onResume()

        val database = FirebaseDatabase.getInstance()

        listener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val favorite = mutableListOf<History>()

                for (favoriteItem in dataSnapshot.children) {
                    val item = favoriteItem.getValue(History::class.java)
                    if (item != null && !favorite.contains(item)) {
                        favorite.add(item)
                    }
                }
                val products = favorite.map {
                    ProductWithFav(it.toProduct(), true)
                }

                val adapter = ListAdapter(products, this@FavoriteProductsFragment)
                list.adapter = adapter

                if (products.isEmpty()) {
                    list.visibility = View.GONE
                    empty_list.visibility = View.VISIBLE
                    scan_empty_txt.setText(R.string.no_fav)
                    button_scan.visibility = View.GONE
                    click_on_button_txt.visibility = View.GONE

                } else {
                    list.visibility = View.VISIBLE
                    empty_list.visibility = View.GONE

                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        }

        reference = database.getReference("Users/$uid/favorites")
        reference!!.addValueEventListener(listener!!)
    }

    override fun onPause() {
        super.onPause()

        if (listener != null) {
            reference?.removeEventListener(listener!!)
        }
    }
}