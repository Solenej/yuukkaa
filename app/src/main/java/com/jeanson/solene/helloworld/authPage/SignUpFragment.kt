package com.jeanson.solene.helloworld.authPage

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.Constraints
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.jeanson.solene.helloworld.R
import com.jeanson.solene.helloworld.mainPage.MainActivity
import kotlinx.android.synthetic.main.auth_signup_layout.*

class SignUpFragment : Fragment(){

    private lateinit var auth: FirebaseAuth

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.auth_signup_layout, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

    }

    override fun onResume() {
        super.onResume()

        val currentUser = auth.currentUser
        if (currentUser != null){
            val intent = Intent(activity?.applicationContext, MainActivity::class.java)
            startActivity(intent)
            activity?.finish()
        }

        //add new user
        sign_up_button.setOnClickListener {
            auth.createUserWithEmailAndPassword(email_lost_pwd.text.toString(), password_sign_up.text.toString())
                .addOnCompleteListener(activity as AppCompatActivity) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(Constraints.TAG, "createUserWithEmail:success")
                        val user = auth.currentUser
                        if (user != null){
                            FirebaseDatabase.getInstance().getReference("Users/${user.uid}").setValue("")
                            val intent = Intent(activity?.applicationContext, MainActivity::class.java)
                            startActivity(intent)
                            activity?.finish()
                        }

                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(Constraints.TAG, "createUserWithEmail:failure", task.exception)
                        Toast.makeText(activity?.baseContext, "Authentication failed.",
                            Toast.LENGTH_SHORT).show()

                        sign_up_error.visibility = View.VISIBLE
                        sign_up_error.text = getString(R.string.error_auth_sign_up)
                    }

                }
        }

        sign_in_link.setOnClickListener {
            activity?.supportFragmentManager!!.beginTransaction()
                .replace(R.id.main_content, SignInFragment())
                .commitAllowingStateLoss()
        }
    }
}