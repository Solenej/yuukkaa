package com.jeanson.solene.helloworld.detailsPage

import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.jeanson.solene.helloworld.Product
import com.jeanson.solene.helloworld.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.product_layout.*

class ProductDetailsFragment : Fragment() {

    private lateinit var product: Product

    companion object {
        fun newInstance(product: Product) : ProductDetailsFragment {
            val fragment = ProductDetailsFragment()
            val args = Bundle()
            args.putParcelable("product", product)

            fragment.arguments = args

            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.product_layout, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments == null || arguments?.containsKey("product") == false){
            throw Exception("The product is missing")
        }

        this.product = arguments!!.getParcelable<Product>("product")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val soldIn = product.soldIn
        val allergens = product.allergens
        val additives = product.additives

        product_title.text = product.name
        product_brand.text = product.brand?.joinToString()
        product_code.text = Html.fromHtml(getString(R.string.productCode, product.code))
        product_quantity.text = Html.fromHtml(getString(R.string.productQuantity, product.quantity ?: getString(R.string.not_given)))
        product_sold_in.text = Html.fromHtml(getString(R.string.productSoldIn, soldIn?.joinToString() ?: getString(R.string.not_given)))
        product_allergen.text = Html.fromHtml(getString(R.string.productAllergen, allergens?.joinToString()
            ?: getString(R.string.allergens_or_additives_null)))
        product_ingredients.text = Html.fromHtml(getString(
            R.string.productIngredients, product.ingredients?.joinToString()
                ?.replace(Regex("\\s_|\'_|\\(_"), " <b>")?.replace(Regex("_\\s|_,|_\\)"), "</b> ")
                ?: getString(R.string.not_given)))
        product_additive.text = Html.fromHtml(getString(R.string.productAdditive, additives?.map { (k, v) -> "$k : $v" }?.joinToString() ?: getString(R.string.allergens_or_additives_null)))
        Picasso.get().load(product.imageUrl).into(image_product)
        product_nutri_score.setImageResource(resources.getIdentifier(
            "nutri_score_${product.nutriscore}",
            "drawable", activity?.applicationContext?.packageName)
        )
    }
}